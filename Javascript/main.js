//Aufgabe 1
const button = document.getElementById("my-button");
const counterText = document.getElementById("counter");
let counter = 0;

button.addEventListener("click", function () {
    counterText.innerText = ++counter;
});

//Aufgabe 2
let result = document.getElementById("result");
let num1 = document.getElementById("num1");
let num2 = document.getElementById("num2");
let buttonadd = document.getElementById("add");
let buttonsub = document.getElementById("sub");
let buttonmul = document.getElementById("mul");
let buttondiv = document.getElementById("div");

buttonadd.addEventListener("click", function () {
    result.value = parseInt(num1.value) + parseInt(num2.value);
    if (isNaN(parseFloat(num1.value)) || isNaN(parseFloat(num2.value))) {
        alert("Bitte Zahlen eingeben");
    }
});

buttonsub.addEventListener("click", function () {
    result.value = parseInt(num1.value) - parseInt(num2.value);
    if (isNaN(parseFloat(num1.value)) || isNaN(parseFloat(num2.value))) {
        alert("Bitte Zahlen eingeben");
    }
});

buttonmul.addEventListener("click", function () {
    result.value = parseInt(num1.value) * parseInt(num2.value);
    if (isNaN(parseFloat(num1.value)) || isNaN(parseFloat(num2.value))) {
        alert("Bitte Zahlen eingeben");
    }
});

buttondiv.addEventListener("click", function () {
    result.value = parseInt(num1.value) / parseInt(num2.value);
    if (isNaN(parseFloat(num1.value)) || isNaN(parseFloat(num2.value))) {
        alert("Bitte Zahlen eingeben");
    }
    if (num2.value != 0) {
        result.value = parseFloat(num1.value) / parseFloat(num2.value);
    } else {
        result.value = 0;
        alert("Division durch 0 nicht möglich!");
    }
});

//Aufgabe 3
let textfield = document.getElementById("textfield");
let textbutton = document.getElementById("textbutton");
let textempty = document.getElementById("textempty");
let textresult = document.getElementById("textresult");
let removeChar = document.getElementById("removeChar");

textbutton.addEventListener("click", function () {
    textresult.value += textfield.value;
});
textempty.addEventListener("click", function () {
    textresult.value = "";
    textfield.value = "";

});
removeChar.addEventListener("click", function () {
    textresult.value = textresult.value.slice(0, - 1);
})

//Aufgabe Farbenmixer
let red = document.getElementById("red");
let green = document.getElementById("green");
let blue = document.getElementById("blue");
let mixedColor = document.getElementById("mixedColor");
let mixColors = document.getElementById("mixColors")

mixColors.addEventListener("click", function () {
    if (red.checked && green.checked && blue.checked) {
        mixedColor.style.background = "white"
    } else if (red.checked && green.checked) {
        mixedColor.style.background = "yellow"
    } else if (red.checked && blue.checked) {
        mixedColor.style.background = "magenta"
    } else if (red.checked) {
        mixedColor.style.background = "red"
    } else if (green.checked && blue.checked) {
        mixedColor.style.background = "cyan"
    } else if (green.checked) {
        mixedColor.style.background = "green"
    } else if (blue.checked) {
        mixedColor.style.background = "blue"
    } else {
        mixedColor.style.background = "black"
    }
})


