
function newPerson(firstName, lastName, birthYear) {
    return {
        firstName,
        lastName,
        birthYear,
        get age() {
            return (new Date().getFullYear() - this.birthYear);
        }
    }
}

const persons = []

function addPerson() {
    let firstName = document.getElementById("firstName").value;
    let lastName = document.getElementById("lastName").value;
    let birthYear = document.getElementById("birthYear").value;
    persons.push(newPerson(firstName, lastName, birthYear));
    firstName.value = "";
    lastName.value = "";
    birthYear.value = "";
    updateTable();
}
function updateTable() {
    document.querySelectorAll(".output")?.forEach(element => element.remove())

    persons.forEach(folks => {
        const table = document.getElementById("listBody");
        const tr = document.createElement("tr");
        const tdFirstName = document.createElement("td");
        const tdLastName = document.createElement("td");
        const tdAge = document.createElement("td");
        tr.classList.add("output");
        tdFirstName.innerText = folks.firstName;
        tdLastName.innerText = folks.lastName;
        tdAge.innerText = folks.age;
        tr.appendChild(tdFirstName);
        tr.appendChild(tdLastName)
        tr.appendChild(tdAge)
        table.appendChild(tr);
    });
}
function removeAll() {
    persons.length = 0;
    updateTable();
}
function removeLast() {
    persons.pop();
    updateTable();
}
function sortByAge() {
    persons.sort(function (a, b) { return a.age - b.age });
    updateTable();
}
function sortByLastName() {
    persons.sort(function (a, b) {
        let x = a.lastName.toUpperCase();
        let y = b.lastName.toUpperCase();
        if (x == y) {
            return 0;
        } else if (x > y) {
            return 1;
        } else if (x < y) {
            return -1;
        }
    });
    updateTable();
}

















