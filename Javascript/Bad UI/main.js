//Page 1
let access = document.getElementById("access");
access?.addEventListener("click", () => window.location.href = "page2.html");
access?.addEventListener("mouseover", function() {
  let changeTop = (Math.random() * ($(window).height() - $("button.access").height()));
  let changeLeft = (Math.random() * ($(window).width() - $("button.access").width()));
  $("button.access").css("margin-top", changeTop + "px");
  $("button.access").css("margin-left", changeLeft + "px");
});

//Page 2
const close = document.getElementById("close");
const sorry = document.getElementById("sorry");
const remove = document.getElementById("submit");
const goodLuck = document.getElementById("goodLuck");
const fullName = document.getElementById("fullName");

close?.addEventListener("click", () => window.location.href = "page3.html");

if (sorry) {
  sorry?.addEventListener("mouseover", function () {
    sorry.textContent = "Oooooooops! Click me anyways!";
    setTimeout(() => sorry.textContent = "Click here to continue", 4000)
  }
  )};

remove?.addEventListener("click", function () {
  fullName.value = fullName.value.slice(0, - 1);
  goodLuck.innerHTML = "Oh no! Submit again";
  setTimeout(() => goodLuck.innerHTML = "Good luck :)", 1500)
  if (fullName.value == "") {
    alert("Submitting your name was NOT successful. Please click on Close");
  }
});

//Page 3
let quick = document.getElementById("quick");
quick?.addEventListener("click", () => window.location.href = "page4.html");

//Page 4
const count= document.getElementById("count");
const counterText = document.getElementById("counter");
const complain = document.getElementById("complain");
let counter = 1900;
const confirm = document.getElementById("confirm");
const year = document.getElementById("year");
const enterYear=document.getElementById("enterYear");
const oops = document.getElementById("oops");
const startOver= document.getElementById("startOver");

confirm?.addEventListener("click", function () {
  if (year.options[year.selectedIndex].text !== "2022") {
    alert("Not 2022! Please write year or click on button Select year");
  }
});

enterYear?.addEventListener("click", function(){
  enterYear.disabled = true;
oops.innerHTML = "Oooops, sooorrry!"
});

count?.addEventListener("click", function () {
    counterText.innerText = ++counter;
    if(counter>=1920){
      counterText.innerText += ("- tired already?")
    }
});

complain?.addEventListener("click", function(){
  if (counter!==2022){
    alert("Couldn't confirm actual year. Please click on button Submit anyways");
  }
});

startOver?.addEventListener("click", function (){
  window.location.href = "index.html", alert("Submitting your complaint was NOT successful. Please confirm with OK and try again!")
});

document.getElementById("startOver").style.cursor = "not-allowed";








